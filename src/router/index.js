import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '@/components/Login/loginPage'
import 'font-awesome/css/font-awesome.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: LoginPage
    }
  ]
})

export const login = {
  userName: {
    placeholder: '',
    icon: 'account_box',
    label: 'Username'
  },
  password: {
    placeholder: '',
    icon: 'lock',
    label: 'Password',
    type: 'password'
  },
  rememberCheckbox: {
    placeholder: '',
    icon: 'lock',
    label: 'Remember me'
  },
  organizationLogo: {
    logo: require('@/assets/login_assets/top-logo.png')
  }
}
